(function() {
	angular.module('capsoTestApp')
	.factory('api', apiService);

	function apiService($http, $log, config) {
		$log.info('api Service initialized');
		function getCompanies() {
			return $http({
				method: 'GET',
				url: config.api.companies,
				cache: true
			});
		}

		function getBondMasters() {
			return $http({
				method: 'GET',
				url: config.api.bondMasters,
				cache: true
			});
		}

		return {
			getCompanies: getCompanies,
			getBondMasters: getBondMasters
		}
	}
})();
