'use strict';

angular.module('capsoTestApp')
.factory('config', function() {
	return {
		api: {
			companies: 'https://s3-eu-west-1.amazonaws.com/capso.test.data/companies.json',
			bondmasters: 'https://s3-eu-west-1.amazonaws.com/capso.test.data/bondmaster.json'
		}
	}
});
