'use strict';

angular.module('capsoTestApp')
.factory('error', function() {
	var error = null;

	function displayError(content, time) {
		error = content;
	}

	return {
		displayError: displayError,
		content: error
	};
});
