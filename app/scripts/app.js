'use strict';

/**
 * @ngdoc overview
 * @name capsotestApp
 * @description
 * # capsotestApp
 *
 * Main module of the application.
 */
angular.module('capsotestApp', [
	'ui.router',
	'ngAnimate',
	'ngResource',
	'ngSanitize',
	'ngTouch'
]).config(function($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('main', {
			url: '/',
			templateUrl: 'views/main.html',
			controller: 'MainCtrl'
		})
		.state('companies', {
			url: '/companies',
			templateUrl: 'views/companies.html',
			controller: 'companiesCtrl'
		});
});
