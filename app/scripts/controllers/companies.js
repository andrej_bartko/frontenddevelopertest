'use strict';

angular.module('capsoTestApp')
.controller('companiesCtrl', function(apiService, $scope, errors) {
	var vm = this;
	vm.companies = [];
	vm.error = null;
	
	function init() {
		apiService.getCompanies()
		.then(handleData)
		.catch(handleError);
	}

	function handleData(data) {
		if (data && data.length > 0) {
			vm.companies = data;
		}
	}

	function handleError(err) {
		vm.error = err;
		// displays error in directove error-display
		errors.display(err, 2000);
	}

});
