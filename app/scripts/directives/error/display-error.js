'use strict';

angular.module('capsoTestApp')
.directive('displayError', function() {
	return {
		restrict: 'AE',
		controllerAs: 'vm',
		templateUrl: '/app/scripts/directives/display-error/display.error.html',
		controller: function(error, $scope) {
			// binding to service
			$scope.errorMessage = error.content
		}
	};
});
